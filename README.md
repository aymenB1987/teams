re# teams


## Getting Started


Ce projet consiste à afficher la liste des teams et le detai de chaque team.
La clean architecture est notre architecture choisie qui permet d'avoir une application maintenable, robuste et facile à tester et qui consiste à organiser notre projet en trois couches:
-domaine: nout permet de définir nos objects logique et notre logique métier.
-data: comprend nos données via serveur ou cache. et permet d'envoyer à la couche domain nos entités.
-presentation: contient nos UI.


## TODO 

- [x] Mise en place de la clean architecture
- [x] Implémenter notre couche domain(entities,repository,use case)
- [x] Mise en place du corps de l'application(erreur, network,theme..)
- [x] Intégrer la gestion des erreurs
- [x] Implémenter notre couche data(models)
- [x] Intégrer le check connexion
- [x] Intégrer Chooper
- [x] Mise en place de notre contrat(repositoryImpl)
- [x] Tester notre repository
- [x] Implémenter notre couche presentation
- [x] Intégrer Bloc
- [ ] Tester notre Bloc
- [ ] Mise en place des Ui

## Principe

### Prerequis
- Mockito: pour les test unitaires
- Chooper : Similaire à Retrofit(Architecute Component) pour Android permet nos appels Api.
- Get it : utiliser pour les injections des dépendanceset utile pour notre gestion de mémoire...
- Bloc : Pattern desing permet de séparer notre logique métier à nos interfaces et utile pour organiser nos états et avoir une interface plus fluide.

