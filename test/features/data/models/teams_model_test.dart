import 'dart:convert';
import 'package:flutter_test/flutter_test.dart';
import 'package:teams/features/team/data/models/Teams/teams_model.dart';
import 'package:teams/features/team/domain/entities/teams.dart';

import '../../../fixtures/fixture_reader.dart';

void main() {
  final teamsModel = TeamsModel();

  test("shoudl be a subclass of Post entity", () async {
    expect(teamsModel, isA<TeamsEntity>());
  });

  group('fromJson', () {
    test("should be a valid model", () async {
      final Map<String, dynamic> jsonMap = json.decode(fixture('teams.json'));
      final result = TeamsModel.fromJson(jsonMap);
      expect(result, teamsModel);
    });
  });
}
