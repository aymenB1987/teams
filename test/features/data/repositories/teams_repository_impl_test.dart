import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:teams/core/network/network_info.dart';
import 'package:teams/features/team/data/datasources/remote/team_endpoint.dart';
import 'package:teams/features/team/data/models/Teams/teams_model.dart';
import 'package:teams/features/team/data/repositories/teams_repository_impl.dart';

import '../../../fixtures/fixture_reader.dart';

class MockTeamEndPoint extends Mock implements TeamEndPoint {}

class MockNetworkInfo extends Mock implements NetworkInfo {}

void main() {
  TeamsRepositoryImpl repository;
  MockTeamEndPoint mockTeamEndPoint;
  MockNetworkInfo mockNetworkInfo;

  setUp(() {
    mockTeamEndPoint = MockTeamEndPoint();
    mockNetworkInfo = MockNetworkInfo();

    repository = TeamsRepositoryImpl(
        teamEndPoint: mockTeamEndPoint, networkInfo: mockNetworkInfo);
  });

  final Map<String, dynamic> jsonMap = json.decode(fixture('teams.json'));
  final teamsModel = TeamsModel.fromJson(jsonMap);
  final teams = teamsModel;

  void runTestsOnline(Function body) {
    group('device is online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });

      body();
    });
  }

  runTestsOnline(() {
    test('should check if the device is online', () async {
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      repository.getRessources();
      verifyNever(mockNetworkInfo.isConnected);
    });

    test('should return a remote data when the call to remote is succfull',
        () async {
      //when(mockTeamEndPoint.getRessources()).thenAnswer((_) async => true );
      final result = await repository.getRessources();
      verifyNever(mockTeamEndPoint.getRessources());
      expect(result, equals(Right(teams)));
    });
  });
}
