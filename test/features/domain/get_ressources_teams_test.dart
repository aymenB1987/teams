import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:teams/features/team/domain/entities/teams.dart';
import 'package:teams/features/team/domain/repositories/teams_repository.dart';
import 'package:teams/features/team/domain/usecases/get_ressources_teams.dart';

class MockTeamsRepository extends Mock implements TeamsRepository {}

void main() {
  GetRessourcesTeams usecase;
  MockTeamsRepository mockPostRepository;
  setUp(() {
    mockPostRepository = MockTeamsRepository();
    usecase = GetRessourcesTeams(mockPostRepository);
  });

  final TeamsEntity teams = TeamsEntity();
  test('should get ressources for the post from the repository', () async {
    when(mockPostRepository.getRessources())
        .thenAnswer((_) async => Right(teams));
    final result = await usecase.execute();
    expect(result, Right(teams));
    verify(mockPostRepository.getRessources());
    verifyNoMoreInteractions(mockPostRepository);
  });
}
