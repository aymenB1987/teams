import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:teams/features/team/presentation/pages/teams_list_page.dart';

class AppApplication extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _application(context);
  }

  Widget _application(BuildContext context) {
    return MaterialApp(
      title: 'Teams',
      theme: ThemeData(
        primaryColor: Colors.green.shade800,
        accentColor: Colors.green.shade600,
      ),
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {'/': (context) => TeamsPage()},
    );
  }
}
