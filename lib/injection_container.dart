import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:get_it/get_it.dart';
import 'package:teams/features/team/data/repositories/teams_repository_impl.dart';
import 'package:teams/features/team/domain/repositories/teams_repository.dart';
import 'package:teams/features/team/domain/usecases/get_ressources_teams.dart';
import 'package:teams/features/team/presentation/bloc/team_bloc.dart';
import 'core/network/network_info.dart';

final sl = GetIt.instance;

Future<void> init() async {
  // Bloc
  sl.registerFactory(
    () => TeamBloc(getTeams: sl()),
  );
  // Use cases
  sl.registerLazySingleton(() => GetRessourcesTeams(sl()));

  // Repository
  sl.registerLazySingleton<TeamsRepository>(
    () => TeamsRepositoryImpl(
      networkInfo: sl(),
    ),
  );

  //! Core
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));

  //! External
  sl.registerLazySingleton(() => DataConnectionChecker());
}
