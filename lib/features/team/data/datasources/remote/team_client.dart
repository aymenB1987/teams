import 'package:chopper/chopper.dart';

class TeamClient {
  static ChopperClient singleton;
  static ChopperClient intance() {
    if (singleton == null) {
      singleton = ChopperClient(
        baseUrl: "http://127.0.0.1:8080/team-api/",
        interceptors: [CurlInterceptor(), HttpLoggingInterceptor()],
        converter: JsonConverter(),
      );
    }
    return singleton;
  }
}
