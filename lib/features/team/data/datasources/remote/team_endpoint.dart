import 'package:chopper/chopper.dart';
import 'package:teams/features/team/data/datasources/remote/team_client.dart';
part 'team_endpoint.chopper.dart';

@ChopperApi(baseUrl: "teams/")
abstract class TeamEndPoint extends ChopperService {
  @Get()
  Future<Response> getRessources();

  static TeamEndPoint create() => _$TeamEndPoint(TeamClient.intance());
}
