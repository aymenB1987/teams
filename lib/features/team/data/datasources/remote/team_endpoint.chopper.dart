// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'team_endpoint.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$TeamEndPoint extends TeamEndPoint {
  _$TeamEndPoint([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = TeamEndPoint;

  @override
  Future<Response<dynamic>> getRessources() {
    final $url = 'teams/';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }
}
