import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';
import 'package:teams/core/error/exceptions.dart';
import 'package:teams/core/error/failures.dart';
import 'package:teams/core/network/network_info.dart';
import 'package:teams/features/team/data/datasources/remote/team_endpoint.dart';
import 'package:teams/features/team/data/models/Teams/teams_model.dart';
import 'package:teams/features/team/domain/entities/teams.dart';
import 'package:teams/features/team/domain/repositories/teams_repository.dart';

class TeamsRepositoryImpl extends TeamsRepository {
  TeamEndPoint teamEndPoint;
  final NetworkInfo networkInfo;
  TeamsRepositoryImpl({
    this.teamEndPoint,
    @required this.networkInfo,
  });

  @override
  Future<Either<Failure, TeamsEntity>> getRessources() async {
    try {
      teamEndPoint = TeamEndPoint.create();
      var response = await teamEndPoint.getRessources();
      print('response api_teams : ${response.body}');
      TeamsEntity teams = TeamsModel.fromJson(response.body);
      return Right(teams);
    } on ServerException {
      return Left(ServerFailure());
    }
  }
}
