// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'teams_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TeamsModel _$TeamsModelFromJson(Map<String, dynamic> json) {
  return TeamsModel(
    teams: (json['teams'] as List)
        ?.map((e) =>
            e == null ? null : TeamModel.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$TeamsModelToJson(TeamsModel instance) =>
    <String, dynamic>{
      'teams': instance.teams,
    };
