import 'package:teams/features/team/data/models/Team/team_model.dart';
import 'package:teams/features/team/domain/entities/teams.dart';

import 'package:json_annotation/json_annotation.dart';
part 'teams_model.g.dart';

@JsonSerializable()
class TeamsModel extends TeamsEntity {
  final List<TeamModel> teams;
  TeamsModel({this.teams}) : super(teams: teams);
  factory TeamsModel.fromJson(Map<String, dynamic> json) =>
      _$TeamsModelFromJson(json);
  Map<String, dynamic> toJson() => _$TeamsModelToJson(this);
}
