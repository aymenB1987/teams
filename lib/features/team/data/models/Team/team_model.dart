import 'package:teams/features/team/data/models/teamMember/team_member_model.dart';
import 'package:teams/features/team/domain/entities/team.dart';
import 'package:json_annotation/json_annotation.dart';
part 'team_model.g.dart';

@JsonSerializable()
class TeamModel extends TeamEntity {
  final String id;
  final String name;
  final List<TeamMemberModel> members;

  TeamModel({this.id, this.name, this.members})
      : super(id: id, name: name, members: members);

  factory TeamModel.fromJson(Map<String, dynamic> json) =>
      _$TeamModelFromJson(json);
  Map<String, dynamic> toJson() => _$TeamModelToJson(this);
}
