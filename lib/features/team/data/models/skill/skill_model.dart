import 'package:flutter/cupertino.dart';
import 'package:teams/features/team/domain/entities/skill.dart';
import 'package:json_annotation/json_annotation.dart';
part 'skill_model.g.dart';

@JsonSerializable()
class SkillModel extends SkillEntity {
  final String id;
  final String name;
  final int rating;
  final int ratingMax;

  SkillModel(
      {this.id, @required this.name, @required this.rating, this.ratingMax})
      : super(id: id, rating: rating, name: name, ratingMax: ratingMax);

  factory SkillModel.fromJson(Map<String, dynamic> json) =>
      _$SkillModelFromJson(json);
  Map<String, dynamic> toJson() => _$SkillModelToJson(this);
}
