import 'package:teams/features/team/data/models/skill/skill_model.dart';
import 'package:teams/features/team/domain/entities/team_member.dart';
import 'package:json_annotation/json_annotation.dart';
part 'team_member_model.g.dart';

@JsonSerializable()
class TeamMemberModel extends TeamMemberEntity {
  final String id;
  final String name;
  final String firstName;
  final int age;
  final String role;
  final List<SkillModel> skills;

  TeamMemberModel({
    this.id,
    this.name,
    this.firstName,
    this.age,
    this.role,
    this.skills,
  }) : super(
            id: id,
            name: name,
            firstName: firstName,
            age: age,
            role: role,
            skills: skills);

  factory TeamMemberModel.fromJson(Map<String, dynamic> json) =>
      _$TeamMemberModelFromJson(json);
  Map<String, dynamic> toJson() => _$TeamMemberModelToJson(this);
}
