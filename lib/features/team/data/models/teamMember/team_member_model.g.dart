// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'team_member_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TeamMemberModel _$TeamMemberModelFromJson(Map<String, dynamic> json) {
  return TeamMemberModel(
    id: json['id'] as String,
    name: json['name'] as String,
    firstName: json['firstName'] as String,
    age: json['age'] as int,
    role: json['role'] as String,
    skills: (json['skills'] as List)
        ?.map((e) =>
            e == null ? null : SkillModel.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$TeamMemberModelToJson(TeamMemberModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'firstName': instance.firstName,
      'age': instance.age,
      'role': instance.role,
      'skills': instance.skills,
    };
