import 'package:dartz/dartz.dart';
import 'package:teams/core/error/failures.dart';
import 'package:teams/features/team/domain/entities/teams.dart';
import 'package:teams/features/team/domain/repositories/teams_repository.dart';

class GetRessourcesTeams {
  final TeamsRepository repository;
  GetRessourcesTeams(this.repository);

  Future<Either<Failure, TeamsEntity>> execute() async {
    return await repository.getRessources();
  }
}
