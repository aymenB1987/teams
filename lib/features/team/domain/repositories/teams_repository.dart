import 'package:dartz/dartz.dart';
import 'package:teams/core/error/failures.dart';
import 'package:teams/features/team/domain/entities/teams.dart';

abstract class TeamsRepository {
  Future<Either<Failure, TeamsEntity>> getRessources();
}
