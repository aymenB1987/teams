import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class SkillEntity extends Equatable {
  final String id;
  final String name;
  final int rating;
  final int ratingMax;

  SkillEntity(
      {this.id, @required this.name, @required this.rating, this.ratingMax});

  @override
  List<Object> get props => [id, name, rating, ratingMax];
}
