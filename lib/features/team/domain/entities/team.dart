import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:teams/features/team/domain/entities/team_member.dart';

class TeamEntity extends Equatable {
  final String id;
  final String name;
  final List<TeamMemberEntity> members;

  TeamEntity({this.id, @required this.name, @required this.members});

  @override
  List<Object> get props => [id,name,members];
}
