import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:teams/features/team/domain/entities/skill.dart';

class TeamMemberEntity extends Equatable {
  final String id;
  final String name;
  final String firstName;
  final int age;
  final String role;
  final List<SkillEntity> skills;

  TeamMemberEntity({
    this.id,
    @required this.name,
    @required this.firstName,
    @required this.age,
    @required this.role,
    @required this.skills,
  });

  @override
  List<Object> get props => [id, name, firstName, age, role, skills];
}
