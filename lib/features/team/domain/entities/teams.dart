import 'package:equatable/equatable.dart';
import 'package:teams/features/team/domain/entities/team.dart';

class TeamsEntity extends Equatable {
  final List<TeamEntity> teams;
  TeamsEntity({this.teams});

  @override
  List<Object> get props => [teams];
}
