import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teams/features/team/presentation/bloc/team_bloc.dart';
import 'package:teams/features/team/presentation/widgets/build_initial_team.dart';
import 'package:teams/features/team/presentation/widgets/build_list_teams.dart';
import 'package:teams/features/team/presentation/widgets/build_loading.dart';
import 'package:teams/injection_container.dart';

class TeamsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Teams"),
      ),
      body: Center(
        child: buildBody(context),
      ),
    );
  }

  BlocProvider<TeamBloc> buildBody(BuildContext context) {
    return BlocProvider(
      create: (_) => sl<TeamBloc>(),
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 16.0),
        alignment: Alignment.center,
        child: BlocListener<TeamBloc, TeamState>(listener: (context, state) {
          if (state is StateError) {
            Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Text(state.message),
              ),
            );
          }
        }, child: BlocBuilder<TeamBloc, TeamState>(
          builder: (context, state) {
            if (state is InitialState) {
              return buildInitialTeams(context);
            } else if (state is LoadingState) {
              return buildLoading();
            } else if (state is LoadedState) {
              return buildListTeams(context, state.teams);
            } else if (state is StateError) {
              return buildInitialTeams(context);
            } else {
              return buildInitialTeams(context);
            }
          },
        )),
      ),
    );
  }
}
