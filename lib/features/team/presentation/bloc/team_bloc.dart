import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:teams/core/error/failures.dart';
import 'package:teams/features/team/domain/entities/teams.dart';
import 'package:teams/features/team/domain/usecases/get_ressources_teams.dart';
part 'team_event.dart';
part 'team_state.dart';

const String SERVER_FAILURE_MESSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';

class TeamBloc extends Bloc<TeamEvent, TeamState> {
  final GetRessourcesTeams getRessourcesTeams;
  TeamBloc({
    @required GetRessourcesTeams getTeams,
  })  : assert(getTeams != null),
        getRessourcesTeams = getTeams;

  @override
  TeamState get initialState => InitialState();

  //TeamBloc() : super(TeamInitial());
  @override
  Stream<TeamState> mapEventToState(
    TeamEvent event,
  ) async* {
    if (event is GetTeamsEvent) {
      yield LoadingState();
      final failureOrTeams = await getRessourcesTeams.execute();
      yield* _eitherLoadedOrErrorState(failureOrTeams);
    }
  }

  Stream<TeamState> _eitherLoadedOrErrorState(
    Either<Failure, TeamsEntity> failureOrTeams,
  ) async* {
    yield failureOrTeams.fold(
      // if Left
      (failure) => StateError(message: _mapFailureToMessage(failure)),
      // if Right
      (teams) => LoadedState(teams: teams),
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return CACHE_FAILURE_MESSAGE;
      default:
        return 'Unexpected error';
    }
  }
}
