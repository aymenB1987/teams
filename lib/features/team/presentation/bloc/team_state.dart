part of 'team_bloc.dart';

@immutable
abstract class TeamState extends Equatable {
  @override
  List<Object> get props => [];
}

class InitialState extends TeamState {}

class LoadingState extends TeamState {}

class LoadedState extends TeamState {
  final TeamsEntity teams;
  LoadedState({
    @required this.teams,
  });

  @override
  List<Object> get props => [teams];
}

class StateError extends TeamState {
  final String message;

  StateError({
    @required this.message,
  });

  @override
  List<Object> get props => [message];
}
