part of 'team_bloc.dart';

@immutable
abstract class TeamEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class GetTeamsEvent extends TeamEvent {
  GetTeamsEvent();
  @override
  List<Object> get props => [];
}
