import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teams/features/team/presentation/bloc/team_bloc.dart';

Widget buildInitialTeams(BuildContext context) {
  return Center(
      child: RaisedButton(
          child: Text('voir ma liste'),
          onPressed: () {
            BlocProvider.of<TeamBloc>(context).add(GetTeamsEvent());
          }));
}
