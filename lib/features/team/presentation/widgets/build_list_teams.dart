import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:teams/features/team/domain/entities/teams.dart';

Widget buildListTeams(BuildContext context, TeamsEntity teams) {
  return Expanded(
      child: ListView.builder(
          itemCount: teams.teams.length,
          itemBuilder: (context, index) {
            return Row(
              children: [
                Text(
                  teams.teams[index].name,
                  style: TextStyle(color: Colors.black),
                ),
              ],
            );
          }));
}
