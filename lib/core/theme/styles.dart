import 'package:flutter/material.dart';

import 'colors.dart';

class AppStyles {
  static const baseline2 = TextStyle(
    fontFamily: "Muli",
    fontSize: 34,
    fontWeight: FontWeight.w900,
    color: AppColors.black,
    height: 37 / 34,
    letterSpacing: 0,
  );
  //Zeplin "Title 2"
  static const title3 = TextStyle(
    fontFamily: "Muli",
    fontSize: 34,
    fontWeight: FontWeight.w900,
    color: AppColors.black,
    letterSpacing: 0,
  );
  static const baseline1 = TextStyle(
    fontFamily: "Muli",
    fontSize: 30,
    fontWeight: FontWeight.w300,
    color: AppColors.black,
    height: 40 / 30,
    letterSpacing: 0,
  );
  //Zeplin "Title2"
  static const title2 = TextStyle(
    fontFamily: "Muli",
    fontSize: 28,
    fontWeight: FontWeight.w900,
    color: AppColors.black,
    letterSpacing: 0,
  );
  static const result = TextStyle(
    fontFamily: "Muli",
    fontSize: 24,
    fontWeight: FontWeight.w900,
    color: AppColors.black,
    letterSpacing: 0,
  );
  static const title = TextStyle(
    fontFamily: "Muli",
    fontSize: 24,
    fontWeight: FontWeight.w800,
    color: AppColors.black,
    height: 30 / 24,
    letterSpacing: 0,
  );
  static const descriptionArticle = TextStyle(
    fontFamily: "Muli",
    fontSize: 20,
    fontWeight: FontWeight.w600,
    color: AppColors.black,
    height: 26 / 20,
    letterSpacing: 0,
  );
  static const onboarding = TextStyle(
    fontFamily: "Muli",
    fontSize: 18,
    fontWeight: FontWeight.bold,
    color: AppColors.black,
    height: 24 / 18,
    letterSpacing: 0,
  );
  static const description = TextStyle(
    fontFamily: "Muli",
    fontSize: 18,
    fontWeight: FontWeight.w500,
    color: AppColors.black,
    height: 22 / 18,
    letterSpacing: 0,
  );
  static const hintText = TextStyle(
    fontFamily: "Muli",
    fontSize: 16,
    fontWeight: FontWeight.w500,
    color: AppColors.dark,
    height: 19 / 16,
    letterSpacing: 0,
  );
  static const onboardingSkip = TextStyle(
    fontFamily: "Muli",
    fontSize: 14,
    fontWeight: FontWeight.bold,
    color: AppColors.steel,
    height: 19 / 14,
    letterSpacing: 0.88,
  );
  static const titleArticle = TextStyle(
    fontFamily: "Muli",
    fontSize: 13,
    fontWeight: FontWeight.bold,
    color: AppColors.cherryRed,
    letterSpacing: 1,
  );
}
