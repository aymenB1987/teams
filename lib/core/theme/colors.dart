import 'package:flutter/material.dart';

class AppColors {
  static const cherryRed = Color(0xFFE2001A);
  static const black = Color(0xFF000000);
  static const paleGrey = Color(0xFFECF1F6);
  static const white = Color(0xFFFFFFFF);
  static const steel = Color(0xFF868B91);
  static const dark = Color(0xFF1A171A);
  static const watermelon = Color(0xFFED5052);
  static const paleGreyTwo = Color(0xFFEBF0F7);
  static const greenyBlue = Color(0xFF58B7B3);
}
