import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:logging/logging.dart';
import 'package:teams/features/team/presentation/pages/teams_list_page.dart';
import 'injection_container.dart' as di;

Future<void> main() async {
  // Fix for defaultBinaryMessenger issue: https://github.com/flutter/flutter/issues/40253
  WidgetsFlutterBinding.ensureInitialized();

  //HttpLoggingInterceptor for finding out detailed information about requests and responses.
  _setupLogging();

  // Initialize Dependency Injection
  await di.init();

  //Portrait mode
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Teams',
      theme: ThemeData(primaryColor: Colors.redAccent),
      home: TeamsPage(),
    );
  }
}

void _setupLogging() {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((rec) {
    print('${rec.level.name}: ${rec.time} : ${rec.message}');
  });
}
